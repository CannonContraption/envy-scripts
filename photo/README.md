# photoorg - a really dumb photo import script

photoorg is a really simple shell script for sorting photos by date and copying
them to the local filesystem. It's designed to have as few moving parts as
possible.

In essence, to use it all you need to do is cd to the directory containing your
camera's mount point, then run 'photoorg' and it'll find and copy all of your
photos to a folder structure (by default in ~/pic) sorted by year, month, then
day.
